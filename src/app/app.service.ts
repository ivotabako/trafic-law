import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Person } from './models/person';


import { Response } from './models/Response';

@Injectable()
export class DataService {

    constructor(private http: HttpClient) { }

    getData(): Observable<Response<Person[]>> {
      return this.http.get<Response<Person[]>>('assets/data.json')
    }
}
