import { Component, OnDestroy, OnInit } from '@angular/core';
import { firstValueFrom, from, Observable, of, Subject, take } from 'rxjs';
import { DataService } from './app.service';
import { Person } from './models/person';
import { Response } from './models/Response';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent implements OnInit {
  title = 'trafic-law';
  public people: Person[] = [];

  public peopleSubject: Subject<Person> = new Subject<Person>();

  constructor(private dataService: DataService) {

  }
  async ngOnInit(): Promise<void> {
    const response = await firstValueFrom(this.dataService.getData());
    this.people = response.data;

    this.peopleSubject?.subscribe( val => {
      this.people.push(val);
    });
  }

  public Add(){
    this.peopleSubject?.next({ name: "kol", age: 19});
  }


}
